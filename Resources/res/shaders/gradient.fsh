#ifdef GL_ES
precision mediump float;
#endif

#define Blend(base, blend, funcf) 	vec4(funcf(base.r, blend.r), funcf(base.g, blend.g), funcf(base.b, blend.b),1.0)
#define BlendNormal(base, blend) 		(blend)
#define BlendOverlay(base, blend) 	Blend(base, blend, BlendOverlayf)
#define BlendOverlayf(base, blend) 	(base < 0.5 ? (2.0 * base * blend) : (1.0 - 2.0 * (1.0 - base) * (1.0 - blend)))

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;


uniform vec4 outsideColor;
uniform vec4 insideColor;
uniform float expand;
uniform vec2 center;
uniform float radius;

highp float random(vec2 co)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

void main(void)
{
    vec4 tex = texture2D(CC_Texture0, v_texCoord);
    
    if(tex.a > 0.0)
    {
        vec2 p = (gl_FragCoord.xy - center) / radius;
        float r = sqrt(dot(p, p));
        
        vec4 color = vec4(0.0);
        
        color = mix(insideColor,outsideColor, (r - expand) / (1.0 - expand));
        
        vec3 noise = vec3(random(v_texCoord * 1.5), random(v_texCoord * 2.5), random(v_texCoord));
        color = mix(color, BlendOverlay(color, noise), 0.07);
        
        gl_FragColor = color;
    }
    
   
}
