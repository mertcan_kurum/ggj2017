varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform float waveLenght;
uniform float speed;

void main() {
   
    vec2 coord = v_texCoord;
    
    float angle = coord.y * 10.0 * waveLenght + mod( CC_Time[1], 0.61 ) * 10.0 * speed ;
    
    coord.x += ( sin(angle) / 30.0 );
    gl_FragColor = texture2D(CC_Texture0, coord);
}