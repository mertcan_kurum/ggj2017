// Shader taken from: http://webglsamples.googlecode.com/hg/electricflower/electricflower.html

#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform vec2 resolution;

const float intensity = 0.05;
vec3 noise(vec2 uv,float time)
{
    vec2 p = abs(sin(uv * 13.0 + uv.x * time * sin(uv.y)));
    
    return vec3(sin (0.2 * time + sin(p * 0.5) * time / cos(50.0)) * 10.0,0.3+0.5 * abs(sin(time * tan(5.0))));
    
}

void main(void)
{
    vec4 tex = texture2D(CC_Texture0, v_texCoord);
    float time = mod(CC_Time[1],45.0) + 10.0;
    if(tex.a == 1.0)
    {
        gl_FragColor.xyz = intensity * noise(gl_FragCoord.xy / sin(resolution.xy * time * 0.01),time) + (1. - intensity) *
        texture2D(CC_Texture0,v_texCoord.xy).xyz;
        gl_FragColor.w = 1.;
    }
    else
    {
        gl_FragColor = tex;
    }
    
}