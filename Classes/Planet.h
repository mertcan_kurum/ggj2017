//
//  Planet.hpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 20/01/17.
//
//

#ifndef Planet_h
#define Planet_h

#include "cocos2d.h"

USING_NS_CC;

class Planet : public cocos2d::Node
{
    
public:
    
    bool initWithFile(std::string fileName);
    static Planet* create(const std::string& filename);
    
    void spendEnergy(float delta);
    float getEnergy();
    
    Sprite* planetSprite;
	void increaseScore();
	int getScore();
    void setEnergy(float energy);
     Sprite* energyBar;
    
private:
    
    void update(float delta) override;
    
   
    
    float currentValue = 180.f,increaseValue=0.4f,decreaseValue=1.2f;

	Label* scoreLabel;

	int score = 0;

	
    
};


#endif /* Planet_hpp */
