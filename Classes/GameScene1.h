#ifndef __Game_SCENE_H__
#define __Game_SCENE_H__

#include "cocos2d.h"
#include "ButtonImage.h"
#include "Planet.h"
#include "TouchableArea.h"
#include "Ball.h"
#include "ensLaserLayer.h"
#include "Wave.h"

USING_NS_CC;

class GameScene1 : public cocos2d::LayerColor
{
public:

	static cocos2d::Scene* createScene();

    bool init() override;
	// a selector callback
	void menuCloseCallback(void* pSender);
    CREATE_FUNC(GameScene1);
	Node* content;

private:
	void update(float delta) override;
	Ball* gameBall = NULL;
	void generateRandomSpaceObject();
	void createLaserEffect(Point startPos, Point endPos);
    Wave* generateWaveObject(int player);

    TouchableArea *touchableTop,*touchableBottom;
    Planet *topPlanet,*bottomPlanet;
    float ballWidth;
	Node* spaceObjectsNode;
	CensLaser* laserEffect;
    Wave* activeTopWave=NULL,*activeBottomWave=NULL;
    int bottomSoundEffect,topSoundEffect;
    bool hasEnd = false;
    
    bool hasCollision(Vec2 pos1,float width1,Vec2 pos2,float width2);
	void startAfterScore(float delta);
	void generateClockShip();
	
	Label* clockLabel = NULL;
	int timer = 75;
	ParticleSystemQuad* meteorParticle;
	void finishGame();
    
    std::vector<Wave*> allWaves;
};

#endif // __Game_SCENE_H__
