//
//  Defines.h
//  HypnoTap
//
//  Created by Mert Can KURUM on 20/09/16.
//
//

#ifndef Defines_h
#define Defines_h

#define GET_WIDTH  Director::getInstance()->getVisibleSize().width
#define GET_HEIGHT Director::getInstance()->getVisibleSize().height
#define GET_DESIGN_WIDTH  540.f
#define GET_DESIGN_HEIGHT 960.f
#define ORIGIN Director::getInstance()->getVisibleOrigin()
#define VISIBLE_SIZE Director::getInstance()->getVisibleSize()
#define WIN_SIZE Director::getInstance()->getWinSize()
#define MINIMUM_OUTERCIRCLE_SIZE 0.05
#define OUTERCIRCLE_RATIO_TO_FIT_IN 0.92
#define DEBUG  true
#define PI 3.14159265

#define FIT_WH(__node__,__width__,__height__) __node__->setScale((__width__)/__node__->getContentSize().width,(__height__)/__node__->getContentSize().height)
#define FIT_W(__node__,__width__) __node__->setScale((__width__)/__node__->getContentSize().width)
#define FIT_H(__node__,__height__) __node__->setScale((__height__)/__node__->getContentSize().height)


static std::string sformat(const std::string fmt_str, ...)
{
    int final_n, n = ((int)fmt_str.size()) * 2;
    std::string str;
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1)
    {
        formatted.reset(new char[n]);
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
}




static float getCurrentAngle(cocos2d::Node* node)
{
    float rotAng = node->getRotation();
    
    if(rotAng >= 180.f)
    {
        rotAng -= 360.f;
    }
    else if (rotAng < -180.f)
    {
        rotAng +=360.f;
    }
    
    // negative angle means node is facing to its left
    // positive angle means node is facing to its right
    return rotAng;
}


static float getAngleDifference(float angle1, float angle2)
{
    float diffAngle = (angle1 - angle2);
    
    if(diffAngle >= 180.f)
    {
        diffAngle -= 360.f;
    }
    else if (diffAngle < -180.f)
    {
        diffAngle +=360.f;
    }
    
    // how much angle the node needs to rotate
    return diffAngle;
}


static float getAngleOfTwoVectors(cocos2d::Point vec1, cocos2d::Point vec2)
{
    auto vectorFromVec1ToVec2 = vec2 - vec1;
    // the angle between two vectors
    return CC_RADIANS_TO_DEGREES(-vectorFromVec1ToVec2.getAngle());
}


static float rotateNodeToPoint(cocos2d::Node* node, cocos2d::Point point)
{
    float angleNodeToRotateTo = getAngleOfTwoVectors(node->getPosition(), point);
    float nodeCurrentAngle = getCurrentAngle(node);
    
    float diffAngle = getAngleDifference(angleNodeToRotateTo, nodeCurrentAngle);
    
    float rotation = nodeCurrentAngle + diffAngle;
    
    node->setRotation(rotation-90);
    
    return rotation;
}
#endif /* Defines_h */
