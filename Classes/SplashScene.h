#ifndef __SplashScene__
#define __SplashScene__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class SplashScene : public cocos2d::Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	void onEnter();
	void finishSplash(float dt);
	CREATE_FUNC(SplashScene);
};

#endif 