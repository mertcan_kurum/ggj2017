#ifndef ShaderNode_h
#define ShaderNode_h

#include "cocos2d.h"

USING_NS_CC;

class ShaderNode : public Node
{
public:
    ~ShaderNode();
    void blur(float radius,float sampleNum);
    void retro();
    void glow();
    
    
    void calculateEdges();
    void visit(cocos2d::Renderer *renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags) override ;
    
    bool init() override;
    CREATE_FUNC(ShaderNode);
   // static ShaderNode* create(const char *pszFileName);
    void setBlurRadius(float radius);
    void setBlurSampleNum(float num);
    void color(Vec4 color);
    void flicker(Vec4 color,float speed,float delay);
    void colorAnimation(Vec4 color,float showTime);
    void shineAnimation(float showTime);
    void wave(float speed,float waveLenght);
    void gray();
    void noise();
    void colorizeWave();
    void bloom();
    void radialGradient(Color3B outerColor,Color3B innerColor);
     Sprite* rendTexSprite;
    
protected:
    float blurRadius;
    float blurSampleNum;
    
    Size getSize();
    Sprite* getSprite();
    
    RenderTexture* renderTexture = NULL;
   
};


#endif /* ShaderBlur_h */
