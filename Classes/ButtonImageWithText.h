#ifndef __OnlineHeadBall__ButtonImageWithText__
#define __OnlineHeadBall__ButtonImageWithText__

#include "ButtonImage.h"

class ButtonImageWithText : public ButtonImage
{
public:
    
    void setOpacity(int value);
    Label* label;
    virtual bool init();
    bool init(std::string fontName);
    
    virtual void labelFitH(float height);
    void setLabelOffset(Vec2 offset);
    void setLabelShadowColor(Color4B color);
    
    CREATE_FUNC(ButtonImageWithText);
    
    static ButtonImageWithText* create(std::string fontNameCustom);
    
    std::string fontName;
    
private:
    
    Color4B shadowColor;
    cocos2d::Size shadowSize;
    
    void updateLabel();
};

#endif
