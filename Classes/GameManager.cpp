#include "GameManager.h"

static GameManager* sharedGameManager = NULL;

GameManager* GameManager::getInstance()
{
	if (!sharedGameManager)
	{
		sharedGameManager = new GameManager();
	}
	return sharedGameManager;
}

GameManager::GameManager() {
	this->score1 = UserDefault::getInstance()->getIntegerForKey("score1", 0);
	this->score2 = UserDefault::getInstance()->getIntegerForKey("score2", 0);
}

int GameManager::getScore1() {
	return score1;
}

int GameManager::getScore2() {
	return score2;
}


void GameManager::setScore1(int value) {
	this->score1 = value;
	UserDefault::getInstance()->setIntegerForKey("score1", value);
}

void GameManager::setScore2(int value) {
	this->score2 = value;
	UserDefault::getInstance()->setIntegerForKey("score2", value);
}