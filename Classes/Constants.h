#ifndef Constants_h
#define Constants_h

static const char* VERSION = "0.1";
static const char* CULTURE = "TR-tr";

static const float FRICTION = 0.005f;

#endif