//
//  Wave.cpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 21/01/17.
//
//

#include "Wave.h"


Wave* Wave::create(const std::string& filename)
{
    Wave *sprite = new (std::nothrow) Wave();
    
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

bool Wave::initWithFile(std::string fileName)
{
    if(!Node::init()) return false;
    
    
    waveSprite = Sprite::create(fileName);
    waveSprite->setAnchorPoint(Vec2(.5f,0));
    waveSprite->setScale(0);
    waveSprite->setOpacity(0);
    addChild(waveSprite);
    
    //waveSprite->runAction(FadeIn::create(.2f));
    
    scheduleUpdate();
    
    return true;
}


void Wave::update(float delta)
{
   
    
}

void Wave::arrangeWithDistance(float distance)
{
    auto scale  = distance*.002f >.4f ? .4f : distance*.002f;
    waveSprite->setScale(scale);
    
    
    auto alpha = scale*255/.4f;
    waveSprite->setOpacity((int)alpha);
}

void Wave::createWave(float delta)
{
    if(fabsf(waveSprite->getScaleY()) < .4f)
        waveSprite->setScale(waveSprite->getScaleY() + (delta*60*increaseValue));
    
    
    if(waveSprite->getOpacity() < 252)
    {
        waveSprite->setOpacity(waveSprite->getOpacity()+4);
    }
    else
    {
        waveSprite->setOpacity(255);
    }
}


void Wave::setPower(float x, float y) {
    this->powerX = x*1.5f;
    this->powerY = y*1.5f;
}

float Wave::getPowerX() {
    return powerX;
}

float Wave::getPowerY() {
    return powerY;
}



void Wave::setSpeed(float x, float y) {
    this->speedX = x;
    this->speedY = y;
}

float Wave::getSpeedX() {
    return speedX;
}

float Wave::getSpeedY() {
    return speedY;
}

void Wave::setPlayer(int player) {
	this->player = player;
}

int Wave::getPlayer() {
	return player;
}
