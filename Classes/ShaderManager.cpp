#include "ShaderManager.h"

static ShaderManager *sharedShaderManager = NULL;

ShaderManager* ShaderManager::getInstance()
{
    if (!sharedShaderManager)
    {
        sharedShaderManager = new ShaderManager();
    }
    return sharedShaderManager;
}

GLProgram* ShaderManager::getOrCreateGlProgram(std::string shaderName)
{
    auto glProgram = GLProgramCache::getInstance()->getGLProgram(shaderName);
    
    if(glProgram == NULL)
    {
        glProgram = new GLProgram();
        glProgram->initWithFilenames("res/shaders/generic.vert", shaderName);
        
        glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        glProgram->link();
        CHECK_GL_ERROR_DEBUG();
        glProgram->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        
        GLProgramCache::getInstance()->addGLProgram(glProgram,shaderName);
    }
    
    return glProgram;
}

GLProgram* ShaderManager::getOrCreateGlProgram(std::string vertexName, std::string shaderName)
{
    auto glProgram = GLProgramCache::getInstance()->getGLProgram(shaderName);
    
    if(glProgram == NULL)
    {
        glProgram = new GLProgram();
        glProgram->initWithFilenames(vertexName, shaderName);
        
        glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        glProgram->link();
        CHECK_GL_ERROR_DEBUG();
        glProgram->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        
        GLProgramCache::getInstance()->addGLProgram(glProgram,shaderName);
    }
    
    return glProgram;
}

void ShaderManager::removeShaderFromSprite(Sprite* sprite)
{
    if(sprite!=NULL)
    {
        sprite->setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR_NO_MVP));
    }
   
}