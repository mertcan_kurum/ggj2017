#include "ShaderNode.h"


ShaderNode::~ShaderNode()
{
    
}

bool ShaderNode::init()
{
    Node::init();
    
    return true;
    
}


void ShaderNode::visit(cocos2d::Renderer *renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags)
{
   // CCASSERT(renderTexture==NULL, "calculateEdges() should be called. RenderTexture is Null");
   
    // quick return if not visible. children won't be drawn.
    if (!_visible || renderTexture == NULL) return;
    
    uint32_t flags = processParentFlags(parentTransform, parentFlags);
    
    // IMPORTANT:
    // To ease the migration to v3.0, we still support the Mat4 stack,
    // but it is deprecated and your code should not rely on it
    _director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    _director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, _modelViewTransform);

    
    bool visibleByCamera = isVisitableByVisitingCamera();
    
    int i = 0;
    
    if(!_children.empty())
    {
        sortAllChildren();
        
        renderTexture->beginWithClear(0, 0, 0, 0);
        
        // draw children zOrder < 0
        for( ; i < _children.size(); i++ )
        {
            auto node = _children.at(i);
            
            if (node && node->getLocalZOrder() < 0 && node != renderTexture)
                node->visit(renderer, _modelViewTransform, flags);
            else
                break;
        }
        
        // self draw
        if (visibleByCamera)
            this->draw(renderer, _modelViewTransform, flags);
        
       
        for(auto it=_children.cbegin()+i; it != _children.cend(); ++it)
        {
            if((*it) != renderTexture)
            {
                (*it)->visit(renderer, _modelViewTransform, flags);
            }
        }
        
        renderTexture->end();
        renderTexture->visit(renderer, _modelViewTransform, flags);
        
    }
    else if (visibleByCamera)
    {
        this->draw(renderer, _modelViewTransform, flags);
    }
    
    _director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);

}

Size ShaderNode::getSize()
{
    return cocos2d::utils::getCascadeBoundingBox(this).size;
}

Sprite* ShaderNode::getSprite()
{
    return renderTexture->getSprite();
}


void ShaderNode::calculateEdges()
{
    if(renderTexture != NULL)
        renderTexture->removeFromParent();
    
    //TODO FIX IT
    auto size = cocos2d::utils::getCascadeBoundingBox(this).size;
    renderTexture = RenderTexture::create(size.width*5, size.height*3);
   // renderTexture->setScale(10);
    addChild(renderTexture);
    
    
}
// SHADERS


//BLUR IMPLEMENTATION

void ShaderNode::blur(float radius,float sampleNum)
{
    blurRadius = radius;
    blurSampleNum = sampleNum;
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/blur.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    renderTexture->getSprite()->setGLProgramState(glProgramState);
    
    auto size = getSize();
    
    getSprite()->getGLProgramState()->setUniformVec2("resolution", size);
    getSprite()->getGLProgramState()->setUniformFloat("blurRadius", blurRadius);
    getSprite()->getGLProgramState()->setUniformFloat("sampleNum", blurSampleNum);
    
 }

void ShaderNode::setBlurRadius(float radius)
{
    blurRadius = radius;
    getSprite()->getGLProgramState()->setUniformFloat("blurRadius", blurRadius);
}

void ShaderNode::setBlurSampleNum(float num)
{
    blurSampleNum = num;
    getSprite()->getGLProgramState()->setUniformFloat("sampleNum", blurSampleNum);
}

// END OF BLUR

void ShaderNode::retro()
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/horizontalColor.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
}

void ShaderNode::color(Vec4 color)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/color.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    getSprite()->getGLProgramState()->setUniformVec4("u_color", color);
    
}

void ShaderNode::flicker(Vec4 color,float speed,float delay)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/flickering.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    getSprite()->getGLProgramState()->setUniformFloat("u_gtime", speed);
    getSprite()->getGLProgramState()->setUniformFloat("u_delay", delay);
    getSprite()->getGLProgramState()->setUniformVec4("u_color", color);
    
}

void ShaderNode::wave(float speed,float waveLenght)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/wave.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    getSprite()->getGLProgramState()->setUniformFloat("waveLenght", waveLenght);
    getSprite()->getGLProgramState()->setUniformFloat("speed", speed);
    
}

void ShaderNode::gray()
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/gray.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    auto size =getSize();
    
    getSprite()->getGLProgramState()->setUniformVec2("resolution", size);
    getSprite()->getGLProgramState()->setUniformVec2("center", Vec2(size.width/2,size.height/2));
    
}


void ShaderNode::radialGradient(Color3B outerColor,Color3B innerColor)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/gradient.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    //getSprite()->getGLProgramState()->setUniformVec2("resolution", size);
    
}

void ShaderNode::noise()
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/noise.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    auto size = getSize();
    
    getSprite()->getGLProgramState()->setUniformVec2("resolution", size);
    
}

void ShaderNode::colorizeWave()
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/colorizeWave.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
}

void ShaderNode::bloom()
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/bloom.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    auto size = getSize();
    
    getSprite()->getGLProgramState()->setUniformVec2("resolution", size);
    
}

void ShaderNode::colorAnimation(Vec4 color,float showTime)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/colorAnim.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    getSprite()->getGLProgramState()->setUniformFloat("u_gtime", showTime);
    getSprite()->getGLProgramState()->setUniformVec4("u_color", color);
    
}

void ShaderNode::shineAnimation(float showTime)
{
    
    std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/shine.fsh"));
    
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());
    
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
    getSprite()->setGLProgramState(glProgramState);
    
    getSprite()->getGLProgramState()->setUniformFloat("u_gtime", showTime);
    
}

// END OF SHADERS

