#ifndef __GameEndScene__
#define __GameEndScene__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class GameEndScene : public cocos2d::Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(GameEndScene);
	void generateRandomSpaceObject();
	void update(float delta) override;
	void startGame(float dt);
};

#endif 