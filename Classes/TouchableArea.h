//
//  TouchableArea.hpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 20/01/17.
//
//

#ifndef TouchableArea_h
#define TouchableArea_h


#include "cocos2d.h"

USING_NS_CC;

class TouchableArea : public cocos2d::Node
{
    
public:
    
    bool init() override;
    CREATE_FUNC(TouchableArea);

    bool getIsHolding();
    
    
    std::function<void(TouchableArea* sender,Touch *position)> mTouchBegin = NULL;
    std::function<void(TouchableArea* sender,Touch *position)> mTouchMove = NULL;
    std::function<void(TouchableArea* sender,Touch *position,Vec2 speed)> mTouchEnd = NULL;
    
    void touchBegin(std::function<void(TouchableArea* sender,Touch* position)> value);
    void touchMove(std::function<void(TouchableArea* sender,Touch *position)> value);
    void touchEnd(std::function<void(TouchableArea* sender,Touch *position,Vec2 speed)> value);

    void releaseWave();
    
    void removeListener();
    
private:
    
    Sprite* touchSprite;
    
    bool onTouchBegan(const std::vector<Touch*>& touches, cocos2d::Event *unused_event);
    void onTouchMoved(const std::vector<Touch*>& touches, cocos2d::Event *unused_event);
    void onTouchEnded(const std::vector<Touch*>& touches, cocos2d::Event *unused_event);
    
    EventListenerTouchAllAtOnce* listener;
    void addListener();

    //double holdTime;
   // bool isHolding = false;
    Touch* currentTouch = NULL;
    
    std::map<int,Touch*> touchDetails;
    
    
};

#endif /* TouchableArea_h */
