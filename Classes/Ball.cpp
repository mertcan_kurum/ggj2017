#include "Ball.h"
#include "CCShake.h"

bool Ball::init()
{
    if(!Node::init()) return false;
    
    
    ballSprite = Sprite::create("res/meteor-ball.png");
    ballSprite->runAction(RepeatForever::create(RotateBy::create(1.f, 4.f)));
    addChild(ballSprite);

    ballSprite->runAction(RepeatForever::create(CCShake::actionWithDuration(.4f, 1.f)));
    
    return true;
}



void Ball::applyForce(float x, float y) {
	this->speedX += x;
	this->speedY += y;
}

void Ball::setSpeed(float x, float y) {
    this->speedX = x;
    this->speedY = y;
}

float Ball::getSpeedX() {
	return speedX;
}

float Ball::getSpeedY() {
	return speedY;
}

void Ball::setLastPlayer(int player) {
	this->lastPlayer = player;
}

int Ball::getLastPlayer() {
	return lastPlayer;
}
