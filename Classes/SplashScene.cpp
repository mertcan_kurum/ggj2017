#include "SplashScene.h"
#include "MenuScene.h"
#include "Defines.h"

using namespace cocos2d;

cocos2d::Scene* SplashScene::createScene()
{
	auto scene = Scene::create();

	auto layer = SplashScene::create();

	scene->addChild(layer);

	return scene;
}

bool SplashScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto sprite = Sprite::create("HelloWorld.png");
	sprite->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	//sprite->setTextureRect(Rect(0, 0, GET_WIDTH / 2, GET_HEIGHT / 2));
	this->addChild(sprite);

	std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/flickering.fsh"));
	auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());

	auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
	sprite->setGLProgramState(glProgramState);
	sprite->getGLProgramState()->setUniformFloat("u_gtime", 1.5f);
	sprite->getGLProgramState()->setUniformVec4("u_color", Vec4(0.5f, 0.5f, 0.5f, 1.0f));
	sprite->getGLProgramState()->setUniformFloat("u_delay", 10.0f);

	return true;
}

void SplashScene::onEnter() {
	Layer::onEnter();

	this->scheduleOnce(schedule_selector(SplashScene::finishSplash), 1.5f);
}

void SplashScene::finishSplash(float dt) {

	auto scene = MenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.6f, scene, Color3B(0, 0, 0)));
}
