//
//  Wave.hpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 21/01/17.
//
//

#ifndef Wave_hpp
#define Wave_hpp

#include "cocos2d.h"

USING_NS_CC;

class Wave : public cocos2d::Node
{
    
public:
    
    bool initWithFile(std::string fileName);
    static Wave* create(const std::string& filename);
    
    void createWave(float delta);
    
    void setSpeed(float x, float y);
    float getSpeedX();
    float getSpeedY();
    
    void setPower(float x, float y);
    float getPowerX();
    float getPowerY();
    
    Sprite* waveSprite;
	int player;

	void setPlayer(int player);
	int getPlayer();
    
    void arrangeWithDistance(float distance);
    
private:
    
    void update(float delta) override;
   
    float increaseValue=0.005f;
    float speedX=0.f,speedY=0.f, powerX=0.f,powerY=0.f;

};

#endif /* Wave_h */
