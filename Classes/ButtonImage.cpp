#include "ButtonImage.h"
#include "SimpleAudioEngine.h"


bool ButtonImage::init()
{
    if (!Node::init())  return false;
    
    mOpacity = 255;
    
    mNode= Node::create();
    
    mSprite = Sprite::create();
    mNode->addChild(mSprite);
    
    listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(ButtonImage::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(ButtonImage::onTouchMoved,this);
    listener->onTouchEnded = CC_CALLBACK_2(ButtonImage::onTouchEnded,this);
    listener->setSwallowTouches(false);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 1);
    
    addChild(mNode);
    
    return true;
}


ButtonImage* ButtonImage::init(std::string file)
{
    mTex = Director::getInstance()->getTextureCache()->addImage(file);
    return init(file,Rect(0, 0, mTex->getContentSize().width, mTex->getContentSize().height));
}

ButtonImage* ButtonImage::init(std::string file, Rect rect)
{
    return init(file, rect, rect);
}

ButtonImage* ButtonImage::init(std::string file, Rect rect,Rect rectHover)
{
    mTex = Director::getInstance()->getTextureCache()->addImage(file);
    mRect = rect;
    mRectHover = rectHover;
    
    mRectHover=rect;
    mSprite->setTexture(mTex);
    mSprite->setTextureRect(rect);
    
    return this;
}

void ButtonImage::disable()
{
    Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
    disabled=true;
}

void ButtonImage::changeTexture(std::string file)
{
    mTex = Director::getInstance()->getTextureCache()->addImage(file);
    mRect = Rect(0, 0, mTex->getContentSize().width, mTex->getContentSize().height);
    mRectHover = mRect;
    
    mSprite->setTexture(mTex);
    mSprite->setTextureRect(mRect);
}

void ButtonImage::changeTexture(std::string file, Rect rect)
{
    changeTexture(file,rect,rect);
}

void ButtonImage::changeTexture(std::string file, Rect rect,Rect rectHover)
{
    mTex = Director::getInstance()->getTextureCache()->addImage(file);
    mRect = rect;
    mRectHover = rectHover;
    
    mSprite->setTexture(mTex);
    mSprite->setTextureRect(rect);
}

void ButtonImage::setTextureRect(Rect rect)
{
    mRectHover=rect;
    mSprite->setTextureRect(rect);
}

void ButtonImage::setPriority(int value)
{
    if(disabled) return;
    
    Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, value);
}

void ButtonImage::onEnter()
{
    Node::onEnter();
    
    if(!disabled) schedule(schedule_selector(ButtonImage::update));
}

void ButtonImage::onExit()
{
    if(!disabled) Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
    
    Node::onExit();
}

void ButtonImage::fitWH(float width,float height)
{
    mSprite->setScale(width/mSprite->getContentSize().width, height/mSprite->getContentSize().height);
}

void ButtonImage::fitH(float height)
{
    mSprite->setScale(height/mSprite->getContentSize().height);
}

void ButtonImage::width(float value)
{
    mSprite->setScaleX(value / mSprite->getBoundingBox().size.width);
    mWidth = value;
}

void ButtonImage::height(float value)
{
    mSprite->setScaleY(value / mSprite->getBoundingBox().size.height);
    mHeight = value;
}

void ButtonImage::setOpacity(float value)
{
    mOpacity=value;
    mSprite->setOpacity(mOpacity);
}

float ButtonImage::getOpacity()
{
    return mOpacity;
}

void ButtonImage::update(float dt)
{
    if (!isVisible()) return;
    
    /*if(Delegate::getInstance()->inTransition)
    {
        mNode->setScale(1.f);
        return;
    }*/
    
    if(dt>.03333) dt=.03333;
    
    if(!doNotAnimate)
    {
        if (mPress)
        {
            mNode->setScale(mNode->getScale()+(pressScaleFactor* 1.125f - mNode->getScale()) * (dt*30.f));
        }
        else
        {
            mNode->setScale(mNode->getScale()+(1.f - mNode->getScale()) * (dt*30.f));
        }
    }
}

bool ButtonImage::isVisible()
{
    return (Node::isVisible() && getParent()!=NULL && getParent()->isVisible());
}

void ButtonImage::setTexture(std::string file)
{
    float scale=mSprite->getScale();
    mSprite->setTexture(file);
    mSprite->setScale(scale);
}

void ButtonImage::setTexture(std::string file,Rect rect)
{
    float scale=mSprite->getScale();
    mSprite->setTexture(file);
    mSprite->setTextureRect(rect);
    mSprite->setScale(scale);
}

void ButtonImage::onHover(std::function<void (ButtonImage *)> value)
{
    mCallbackHover=value;
}

void ButtonImage::onRelease(std::function<void (ButtonImage *)> value)
{
    mCallbackRelease=value;
}

void ButtonImage::onTap(std::function<void (ButtonImage *)> value)
{
    mCallbackTap=value;
}

void ButtonImage::onPress(std::function<void (ButtonImage *)> value)
{
    mCallbackPress=value;
}

void ButtonImage::onPressFilter(std::function<bool (ButtonImage *)> value)
{
    mCallbackPressFilter=value;
}

void ButtonImage::_onRelease()
{
    if(mCallbackRelease!=NULL) mCallbackRelease(this);
    
    mSprite->setTextureRect(mRect);
}

void ButtonImage::cancelTouch()
{
    mPress = false;
    _onRelease();
}

static double lastPressTimeMilliseconds = 0;

bool ButtonImage::canPressButton()
{
    currentTimeMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    
    if(currentTimeMilliseconds-lastPressTimeMilliseconds > 100)
    {
        lastPressTimeMilliseconds=currentTimeMilliseconds;
        return true;
    }
    
    return false;
}

static bool justTouchedOutsideMaskedArea=false;

void ButtonImage::touchedInsideMaskedArea()
{
    justTouchedOutsideMaskedArea=false;
}

void ButtonImage::touchedOutsideMaskedArea()
{
    justTouchedOutsideMaskedArea=true;
}

/////

bool ButtonImage::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if(disabled || !isVisible() /*|| Delegate::getInstance()->inTransition*/) return false;
    
    if (mPress)
    {
        mPress = false;
        _onRelease();
        currentTouch = NULL;
    }
    
    location = convertTouchToNodeSpace(touch);
    
    if (mCallbackPressFilter == NULL || mCallbackPressFilter(this))
    {
        if(mSprite->getBoundingBox().containsPoint(location) ||
           (hasAdditionalTouchRect &&
            Rect(additionalTouchRectNode->getBoundingBox().origin.x,
                 additionalTouchRectNode->getBoundingBox().origin.y-additionalTouchRectNode->getBoundingBox().size.height*.5,
                 additionalTouchRectNode->getBoundingBox().size.width,
                 additionalTouchRectNode->getBoundingBox().size.height*2).containsPoint(location)))
        {
            
            if(isInMaskedArea && justTouchedOutsideMaskedArea)
            {
                justTouchedOutsideMaskedArea=false;
                return false;
            }
            
            if(!canPressButton()) return false;
            
            currentTouch = touch;
            
            mPress = true;
            
            mSprite->setTextureRect(mRectHover);
            
            if(mSprite->isVisible()) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("res/audio/but3.wav");
            
            if(mCallbackHover!=NULL) mCallbackHover(this);
            if(mCallbackPress!=NULL) mCallbackPress(this);
        }
    }
    
    return  true;
}

void ButtonImage::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if (!isVisible() || touch != currentTouch) return;
    
    Vec2 location = convertTouchToNodeSpace(touch);
    
    if(mPress && !mSprite->getBoundingBox().containsPoint(location) && (!hasAdditionalTouchRect || !Rect(additionalTouchRectNode->getBoundingBox().origin.x,
                                                                                                         additionalTouchRectNode->getBoundingBox().origin.y-additionalTouchRectNode->getBoundingBox().size.height*.5,
                                                                                                         additionalTouchRectNode->getBoundingBox().size.width,
                                                                                                         additionalTouchRectNode->getBoundingBox().size.height*2).containsPoint(location)))
    {
        currentTouch = NULL;
        mPress=false;
        _onRelease();
    }
}

void ButtonImage::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if (!isVisible() || touch != currentTouch) return;
    
    if (mPress)
    {
        currentTouch = NULL;
        mPress = false;
        _onRelease();
        
        if(!doNotPlaySoundOnRelease) CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("res/audio/but2.wav");
        
        if(mCallbackTap!=NULL) mCallbackTap(this);
    }
}
