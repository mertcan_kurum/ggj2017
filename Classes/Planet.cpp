//
//  Planet.cpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 20/01/17.
//
//

#include "Planet.h"
#include "Defines.h"

USING_NS_CC;

Planet* Planet::create(const std::string& filename)
{
    Planet *sprite = new (std::nothrow) Planet();
    
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

bool Planet::initWithFile(std::string fileName)
{
    if(!Node::init()) return false;
    
    
    planetSprite = Sprite::create(fileName);
    planetSprite->setScale(.7f);
    addChild(planetSprite);
    planetSprite->runAction(RepeatForever::create(RotateBy::create(1.f, 5.f)));
    
    
    energyBar = Sprite::create("res/energy.png");
    energyBar->setAnchorPoint(Vec2(.5f,0));
    energyBar->setScale(.7f);
    addChild(energyBar);

	scoreLabel = Label::createWithTTF(sformat("%i", score), "fonts/hunger-games.ttf", 36);
	scoreLabel->setTextColor(Color4B(255, 219, 21, 225));
	scoreLabel->setPositionY(planetSprite->getBoundingBox().size.height * .15f);
	addChild(scoreLabel);
    
    energyBar->setRotation(currentValue - 180.f);
    
    scheduleUpdate();
    
    return true;
}


void Planet::update(float delta)
{
    if(currentValue < 180.f)
    {
        
        currentValue += delta*60*increaseValue;
        currentValue = currentValue > 180.0 ? 180.0 : currentValue;
        
        energyBar->setRotation(currentValue - 180.f);
        
        auto redValue = fabsf(currentValue + 180.f) * 255/180;
        energyBar->setColor(Color3B(255,redValue,redValue));

    }
    
}

float Planet::getEnergy()
{
    return currentValue;
}

void Planet::setEnergy(float energy)
{
    currentValue = energy;
}


void Planet::spendEnergy(float energy)
{
    if(currentValue > energy)
        currentValue -= energy;
    else
        currentValue = 0;
    
    

    auto redValue = fabsf(currentValue + 180.f) * 255/180;
    energyBar->setColor(Color3B(255,redValue,redValue));
    
}

void Planet::increaseScore() {
	score++;
	scoreLabel->setString(sformat("%i", score));
}

int Planet::getScore()
{
	return score;
}
