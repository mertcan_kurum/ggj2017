#ifndef GameManager_h
#define GameManager_h

#include "cocos2d.h"

USING_NS_CC;

class GameManager
{

public:
	GameManager();
	static GameManager* getInstance();
	int getScore1();
	int getScore2();
	void setScore1(int value);
	void setScore2(int value);
	

private:
	int score1 = 0;
	int score2 = 0;
};

#endif