//
//  TouchableArea.cpp
//  GGJ2017
//
//  Created by Mert Can KURUM on 20/01/17.
//
//

#include "TouchableArea.h"
#include "Defines.h"

bool TouchableArea::init()
{
    if(!Node::init()) return false;
    
    touchSprite = Sprite::create("res/touch-field.png");
    FIT_W(touchSprite, GET_DESIGN_WIDTH*1.11f);
    addChild(touchSprite);
    
    addListener();
    
    return true;
}


void TouchableArea::removeListener()
{
    Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
}

void TouchableArea::addListener()
{
    listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan = CC_CALLBACK_2(TouchableArea::onTouchBegan, this);
    listener->onTouchesMoved = CC_CALLBACK_2(TouchableArea::onTouchMoved, this);
    listener->onTouchesEnded = CC_CALLBACK_2(TouchableArea::onTouchEnded, this);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener,this);
}

bool TouchableArea::onTouchBegan(const std::vector<Touch*>& touches, cocos2d::Event *unused_event)
{
    if(getIsHolding()) return false;
    
    for (int i =0; i<touches.size(); i++) {
        
        auto touch = touches.at(i);
        
        auto location = convertTouchToNodeSpace(touch);
        
        auto distanceToCenter = sqrt(location.x*location.x + location.y*location.y);
        
        if(distanceToCenter <= touchSprite->getBoundingBox().size.width*.45f && distanceToCenter > touchSprite->getBoundingBox().size.width*.1f)
        {
            touchDetails[touch->getID()] = touch;
            currentTouch = touch;
            if(mTouchBegin != NULL)
            {
                mTouchBegin(this,touch);
            }
            
            return true;
            
        }
        
        touchDetails[touch->getID()] = NULL;
        
        
    }
   
    
    return  false;
    
}

void TouchableArea::onTouchMoved(const std::vector<Touch*>& touches, cocos2d::Event *unused_event)
{
    if(currentTouch ==NULL) return;
    log("patlama1");
    for (int i =0; i<touches.size(); i++) {
        
        auto touch = touches.at(i);
        
        if(currentTouch->getID() == touch->getID())
        {
               log("patlama2");
            if(mTouchMove != NULL)
            {
                mTouchMove(this,touch);
            }
            break;
        }
    
    }
    
    
}

void TouchableArea::onTouchEnded(const std::vector<Touch*>& touches, cocos2d::Event *unused_event)
{
    releaseWave();
    
}

void TouchableArea::releaseWave()
{
    if(currentTouch ==NULL) return;
      log("patlama3");
    for (int i =0; i<touchDetails.size(); i++) {
        
           log("patlama4");
        auto touch = touchDetails.at(i);
        if(touch == NULL ) continue;
           log("patlama5");
        auto location = convertTouchToNodeSpace(touch);
        
        if(mTouchEnd != NULL)
        {
            mTouchEnd(this,touch,location);
        }
        currentTouch = NULL;
        break;
    }
    
    touchDetails.clear();
}

void TouchableArea::touchBegin(std::function<void (TouchableArea *,Touch* position)> value)
{
    mTouchBegin=value;
}

void TouchableArea::touchMove(std::function<void (TouchableArea *,Touch* position)> value)
{
    mTouchMove=value;
}

void TouchableArea::touchEnd(std::function<void (TouchableArea *,Touch* position,Vec2 speed)> value)
{
    mTouchEnd=value;
}


bool TouchableArea::getIsHolding()
{
    for (int i =0; i<touchDetails.size(); i++) {
        if(touchDetails[i] > 0)
        {
            return true;
        }
    }
    return false;
}
