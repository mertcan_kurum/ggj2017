#ifndef ShaderManager_h
#define ShaderManager_h

#include "cocos2d.h"

USING_NS_CC;

class ShaderManager
{
    
public:

    static ShaderManager* getInstance();
    GLProgram* getOrCreateGlProgram(std::string fileName);
    void removeShaderFromSprite(Sprite* sprite);
    GLProgram* getOrCreateGlProgram(std::string vertexName, std::string shaderName);
};


#endif