#ifndef  _BALL_H_
#define  _BALL_H_

#include "cocos2d.h"

USING_NS_CC;

class  Ball : public cocos2d::Node
{
public:
    
    bool init() override;
    CREATE_FUNC(Ball);
    
	void applyForce(float x, float y);
    void setSpeed(float x, float y);
	float getSpeedX();
	float getSpeedY();
    
    Sprite* ballSprite;

	void setLastPlayer(int player);
	int getLastPlayer();
	
private:
    
    //bool initWithFile(const std::string &filename) override;
    
	float speedX = 0.f;
	float speedY = 0.f;
	int lastPlayer = -1;
};

#endif //
