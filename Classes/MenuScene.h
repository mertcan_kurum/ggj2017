#ifndef __MenuScene__
#define __MenuScene__

#include "cocos2d.h"

USING_NS_CC;

class MenuScene : public cocos2d::Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(MenuScene);
	void generateRandomSpaceObject();
	void update(float delta) override;
	void startGame(float dt);
};

#endif 
