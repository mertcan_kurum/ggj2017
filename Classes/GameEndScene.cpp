#include "GameEndScene.h"
#include "GameScene1.h"
#include "Defines.h"
#include "ButtonImageWithText.h"
#include "GameManager.h";
#include "MenuScene.h";

using namespace cocos2d;

cocos2d::Scene* GameEndScene::createScene()
{
	auto scene = Scene::create();

	auto layer = GameEndScene::create();

	scene->addChild(layer);

	return scene;
}

bool GameEndScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto bg = Sprite::create("res/game-bg.png");
	bg->setPosition(GET_WIDTH / 2, GET_HEIGHT / 2);
	
	addChild(bg);

	ParticleSystemQuad* particle1 = ParticleSystemQuad::create("res/particles/firefly-particle.plist");
	particle1->setPosVar(Vec2(GET_WIDTH, GET_HEIGHT));
	particle1->setColor(Color3B(255, 0, 0));
	addChild(particle1);

	auto planet1 = Sprite::create("res/planet-1.png");
	planet1->setPosition(GET_WIDTH / 2, 0);
	planet1->runAction(RepeatForever::create(RotateBy::create(1.0f, 5.0f)));
	addChild(planet1, 5);

	auto planet2 = Sprite::create("res/planet-2.png");
	planet2->setPosition(GET_WIDTH / 2, GET_HEIGHT);
	planet2->runAction(RepeatForever::create(RotateBy::create(1.0f, -4.0f)));
	addChild(planet2, 5);

	auto scoreLabel = Label::createWithTTF(sformat("%i  :  %i", GameManager::getInstance()->getScore1(), GameManager::getInstance()->getScore2()), "fonts/hunger-games.ttf", 48);
	scoreLabel->setTextColor(Color4B(255, 219, 21, 225));
	scoreLabel->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.725f);
	addChild(scoreLabel);

	planet1->runAction(Sequence::create(DelayTime::create(1.f), EaseOut::create(Spawn::createWithTwoActions((MoveTo::create(.8f, Vec2(0, GET_HEIGHT * .65f))), ScaleTo::create(.75f, .7f)), 2.0f), NULL));
	planet2->runAction(Sequence::create(DelayTime::create(1.f), EaseOut::create(Spawn::createWithTwoActions((MoveTo::create(.8f, Vec2(GET_WIDTH, GET_HEIGHT * .80f))), ScaleTo::create(.75f, .7f)), 2.0f), NULL));

	auto startButton = ButtonImageWithText::create();
	startButton->ButtonImage::init("res/button.png");
	startButton->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.4f);
	startButton->label->setString("RESTART");
	startButton->setLabelOffset(Vec2(startButton->mSprite->getBoundingBox().size.width * .1f, -startButton->mSprite->getBoundingBox().size.height * .02f));
	startButton->label->setTextColor(Color4B(255, 219, 21, 225));
	startButton->setScale(0);
	startButton->labelFitH(startButton->mSprite->getBoundingBox().size.height * .4f);
	startButton->runAction(Sequence::create(DelayTime::create(1.3f),EaseBackOut::create(ScaleTo::create(.3f, 0.7f)),NULL));
	addChild(startButton, 10);

	auto menuButton = ButtonImageWithText::create();
	menuButton->ButtonImage::init("res/button.png");
	menuButton->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.22f);
	menuButton->label->setString("MENU");
	menuButton->setLabelOffset(Vec2(startButton->mSprite->getBoundingBox().size.width * .1f, -startButton->mSprite->getBoundingBox().size.height * .02f));
	menuButton->label->setTextColor(Color4B(255, 219, 21, 225));
	menuButton->labelFitH(startButton->mSprite->getBoundingBox().size.height * .4f);
	menuButton->setScale(0);
	menuButton->runAction(Sequence::create(DelayTime::create(1.4f), EaseBackOut::create(ScaleTo::create(.3f, 0.7f)), NULL));
	addChild(menuButton, 10);

	startButton->onTap([this, planet1, planet2, startButton, menuButton](ButtonImage *sender) {
		sender->mSprite->stopAllActions();
	
		startButton->runAction(EaseBackIn::create(ScaleTo::create(.5f, 0)));
		menuButton->runAction(EaseBackIn::create(ScaleTo::create(.5f, 0)));

		auto touchAnimationBottom = Sprite::create("res/touch-field.png");
		FIT_W(touchAnimationBottom, GET_DESIGN_WIDTH*1.11f);
		touchAnimationBottom->setPosition(GET_WIDTH / 2, 0);

		auto initialScaleBottom = touchAnimationBottom->getScale();
		touchAnimationBottom->setScale(0);
		addChild(touchAnimationBottom);
		touchAnimationBottom->runAction(Sequence::create(DelayTime::create(.3f), EaseBackIn::create(ScaleTo::create(0.4f, initialScaleBottom)),NULL));

		auto touchAnimationTop = Sprite::create("res/touch-field.png");
		FIT_W(touchAnimationTop, GET_DESIGN_WIDTH*1.11f);
		auto initialScaleTop = touchAnimationTop->getScale();

		touchAnimationTop->setPosition(GET_WIDTH / 2, GET_HEIGHT);
		touchAnimationTop->setScale(0);
		addChild(touchAnimationTop);
		touchAnimationTop->runAction(Sequence::create(DelayTime::create(.3f), EaseBackIn::create(ScaleTo::create(0.4f, initialScaleTop)), NULL));

		this->scheduleOnce(schedule_selector(GameEndScene::startGame), .9f);
	});

	menuButton->onTap([this](ButtonImage *sender) {
		auto scene = MenuScene::createScene();
		Director::getInstance()->replaceScene(TransitionFade::create(0.5f, scene));
	});

	scheduleUpdate();
	return true;
}

void GameEndScene::update(float delta) {
	auto value = cocos2d::random() % 500;

	if (value == 1) {
		generateRandomSpaceObject();
	}
}

void GameEndScene::generateRandomSpaceObject() {

	int spaceObjectType = random(0, 7), startHeight = random(0, (int)GET_HEIGHT), endHeight = random(0, (int)GET_HEIGHT), rotation = random(0, 1);
	float scale;
	Sprite* spaceObject;

	if (spaceObjectType == 0) {
		spaceObject = Sprite::create("res/satellite.png");
		scale = 0.1f + fmodf(rand_0_1(), 0.15f);
	}
	else if (spaceObjectType == 1) {
		spaceObject = Sprite::create("res/rocket.png");
		scale = 0.1f + fmodf(rand_0_1(), 0.25f);
	}
	else if (spaceObjectType > 1) {
		spaceObject = Sprite::create(sformat("res/back-meteor-%i.png", spaceObjectType - 1));
		scale = 0.3f + fmodf(rand_0_1(), 0.35f);
		spaceObject->runAction(RepeatForever::create(RotateBy::create(1.0f, 40.0f)));
	}

	spaceObject->setScale(scale);
	spaceObject->setColor(Color3B(182, 182, 182));
	MoveTo* moveAction;
	auto rotationAngle = atan(((float)endHeight - (float)startHeight) / GET_WIDTH) * 180 / PI;

	if (rotation == 0) {
		spaceObject->setPosition(-100, startHeight);
		moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(GET_WIDTH + 100, (float)endHeight));
		spaceObject->setRotation(90 - rotationAngle);
	}
	else {
		spaceObject->setPosition(GET_WIDTH + 100, startHeight);
		moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(-100, (float)endHeight));
		spaceObject->setRotation(-90 + rotationAngle);
	}

	if (scale < 0.2f) {
		addChild(spaceObject, 3);
	}
	else {
		addChild(spaceObject, 6);
	}

	spaceObject->runAction(Sequence::create(moveAction, CallFunc::create([this, spaceObject]() {
		spaceObject->removeFromParent();
	}), NULL));
}


void GameEndScene::startGame(float dt) {
	auto scene = GameScene1::createScene();
	Director::getInstance()->replaceScene(TransitionProgressVertical::create(0.2f, scene));
}
