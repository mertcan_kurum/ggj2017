#include "GameScene1.h"
#include "SimpleAudioEngine.h"
#include "ShaderNode.h"
#include "Defines.h"
#include "CCShake.h"
#include "Ball.h"
#include "Constants.h"
#include "GameEndScene.h"
#include "GameManager.h"

USING_NS_CC;

Scene* GameScene1::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameScene1::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene1::init()
{
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(255, 255, 255, 215)))
    {
        return false;
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("res/audio/explosion.m4a");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("res/audio/filling.wav");
    
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(.2f);
    
    content = Node::create();
    content->setPosition(GET_WIDTH/2,GET_HEIGHT/2);
    addChild(content);
    
    auto bg = Sprite::create("res/game-bg.png");
    //bg->setColor(Color3B(120,120,120));
    content->addChild(bg);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/game-sound.m4a",true);
    
    ParticleSystemQuad* particle1 = ParticleSystemQuad::create("res/particles/firefly-particle.plist");
    particle1->setPosVar(Vec2(GET_WIDTH,GET_HEIGHT));
    particle1->setColor(Color3B(255,0,0));
    addChild(particle1);
    
    spaceObjectsNode = Node::create();
    spaceObjectsNode->setPosition(-GET_WIDTH / 2, -GET_HEIGHT / 2);
    content->addChild(spaceObjectsNode);
    
    auto animationDuration = 3.f;
    auto touchAnimationBottom = Sprite::create("res/touch-field.png");
    FIT_W(touchAnimationBottom, GET_DESIGN_WIDTH*1.11f);
    touchAnimationBottom->setOpacity(120);
    touchAnimationBottom->setPositionY(-GET_HEIGHT/2);
    content->addChild(touchAnimationBottom);
    
    touchAnimationBottom->runAction(RepeatForever::create(Sequence::create(EaseSineIn::create(Spawn::createWithTwoActions(ScaleTo::create(animationDuration, 0), FadeOut::create(animationDuration))),CallFunc::create([this,touchAnimationBottom](){
        
        
        FIT_W(touchAnimationBottom, GET_DESIGN_WIDTH*1.11f);
        
    }),FadeTo::create(1.f,120), NULL)));
    
    
    
    auto touchAnimationTop = Sprite::create("res/touch-field.png");
    FIT_W(touchAnimationTop, GET_DESIGN_WIDTH*1.11f);
    touchAnimationTop->setOpacity(120);
    touchAnimationTop->setPositionY(GET_HEIGHT/2);
    content->addChild(touchAnimationTop);
    
    
    auto spawnAction = Spawn::createWithTwoActions(ScaleTo::create(animationDuration, 0), FadeOut::create(animationDuration));
    touchAnimationTop->runAction(RepeatForever::create(Sequence::create(EaseSineOut::create(spawnAction),CallFunc::create([this,touchAnimationTop](){
        
        touchAnimationTop->setOpacity(120);
        FIT_W(touchAnimationTop, GET_DESIGN_WIDTH*1.11f);
        
    }), NULL)));
    
    
    touchableBottom = TouchableArea::create();
    
    touchableBottom->touchBegin([this](TouchableArea* sender,Touch* touch){
        
        auto location = content->convertTouchToNodeSpace(touch);
        
        auto localPos = sender->convertTouchToNodeSpace(touch);
        
        auto waveCreated = generateWaveObject(1);
        
        waveCreated->setPosition(location);
        content->addChild(waveCreated);
        
        activeBottomWave = waveCreated;
        
        auto rotationAngle = rotateNodeToPoint(activeBottomWave,location);
        activeBottomWave->setSpeed(-cos(rotationAngle/180.0 * PI)*4.5f,sin(rotationAngle/180.0 * PI)*4.5f);
        
    });
    
    touchableBottom->touchMove([this](TouchableArea* sender,Touch* touch){
        
        auto location = content->convertTouchToNodeSpace(touch);
        auto rotationAngle = rotateNodeToPoint(activeBottomWave,location);
        activeBottomWave->setSpeed(-cos(rotationAngle/180.0 * PI)*4.5f,sin(rotationAngle/180.0 * PI)*4.5f);

        
        auto xDiff =(location.x-activeBottomWave->getPositionX());
        auto yDiff =(location.y-activeBottomWave->getPositionY());

        auto distance = sqrt( xDiff*xDiff + yDiff*yDiff);
        activeBottomWave->arrangeWithDistance(distance);
        
    });
    touchableBottom->touchEnd([this](TouchableArea* sender,Touch* touch,Vec2 speed){
        
        
        activeBottomWave->setPower( activeBottomWave->getSpeedX()*activeBottomWave->waveSprite->getOpacity()/255,activeBottomWave->getSpeedY()*activeBottomWave->waveSprite->getOpacity()/255);
        
        allWaves.push_back(activeBottomWave);
        
        bottomPlanet->spendEnergy(activeBottomWave->waveSprite->getScale()*200);
        
    });
    
    touchableBottom->setPositionY(-GET_HEIGHT/2);
    content->addChild(touchableBottom);
    
    touchableTop = TouchableArea::create();
    
    touchableTop->touchBegin([this](TouchableArea* sender,Touch* touch){
        
        auto location = content->convertTouchToNodeSpace(touch);
        
        auto localPos = sender->convertTouchToNodeSpace(touch);
        
        auto waveCreated = generateWaveObject(2);
        
        
        waveCreated->setPosition(location);
        content->addChild(waveCreated);
        
        activeTopWave = waveCreated;
        
        auto rotationAngle = rotateNodeToPoint(activeTopWave,location);
        activeTopWave->setSpeed(-cos(rotationAngle/180.0 * PI)*4.5f,sin(rotationAngle/180.0 * PI)*4.5f);

    });
    
    
    touchableTop->touchMove([this](TouchableArea* sender,Touch* touch){
        
        auto location = content->convertTouchToNodeSpace(touch);
        
        auto rotationAngle = rotateNodeToPoint(activeTopWave,location);
        activeTopWave->setSpeed(-cos(rotationAngle/180.0 * PI)*4.5f,sin(rotationAngle/180.0 * PI)*4.5f);
        
        
        auto xDiff =(location.x-activeTopWave->getPositionX());
        auto yDiff =(location.y-activeTopWave->getPositionY());
        
        auto distance = sqrt( xDiff*xDiff + yDiff*yDiff);
        activeTopWave->arrangeWithDistance(distance);
    });
    
    touchableTop->touchEnd([this](TouchableArea* sender,Touch* touch,Vec2 speed){
        
        activeTopWave->setPower(activeTopWave->getSpeedX()*activeTopWave->waveSprite->getOpacity()/255,activeTopWave->getSpeedY()*activeTopWave->waveSprite->getOpacity()/255);
        allWaves.push_back(activeTopWave);
        
        topPlanet->spendEnergy(activeTopWave->waveSprite->getScale()*200);
        
    });
    
    touchableTop->setPositionY(GET_HEIGHT/2);
    content->addChild(touchableTop);
    
    
    bottomPlanet = Planet::create("res/planet-1.png");
    bottomPlanet->setPositionY(-GET_HEIGHT/2);
    content->addChild(bottomPlanet);
    
    topPlanet = Planet::create("res/planet-2.png");
    topPlanet->setPositionY(GET_HEIGHT/2);
    topPlanet->setScaleY(-topPlanet->getScaleY());
    topPlanet->setScaleX(-topPlanet->getScaleX());
    content->addChild(topPlanet);
    
    gameBall = Ball::create();
    content->addChild(gameBall);
    ballWidth = gameBall->ballSprite->getBoundingBox().size.width;
    
    gameBall->setScale(0);
    gameBall->runAction(Sequence::create(DelayTime::create(1.f), CallFunc::create([this]() {
        gameBall->runAction(Sequence::create(EaseBackOut::create(ScaleTo::create(.2f, .9f)), CallFunc::create([this]() {
            gameBall->applyForce(cocos2d::random(0, 100) / 75.f * cocos2d::rand_minus1_1(), cocos2d::random(0, 100) / 75.f * cocos2d::rand_minus1_1());
        }), NULL));
    }), NULL));
    
    scheduleUpdate();
    
    meteorParticle = new ParticleSystemQuad();
    meteorParticle = ParticleMeteor::create();
    auto tempSprite = Sprite::create("res/fire.png");
    meteorParticle->setTexture(tempSprite->getTexture());
    meteorParticle->setStartSize(100);
    meteorParticle->setEndSize(80);
    meteorParticle->setGravity(Vec2(0, 0));
    meteorParticle->setLife(1.f);
    meteorParticle->setEmissionRate(80);
    //meteorParticle->setLifeVar(2.f);
    meteorParticle->setPositionType(ParticleSystem::PositionType::FREE);
    meteorParticle->setPosition(gameBall->getPositionX() * 0.25, gameBall->getPositionY() * 0.25);
    //meteorParticle->setSourcePosition(gameBall->getPosition());
    meteorParticle->setRotation(gameBall->getRotation() * -1.f);
	meteorParticle->setStartColor(Color4F(.5f, .5f, .5f, .15f));
    gameBall->addChild(meteorParticle);
    
    createLaserEffect(Point(-GET_DESIGN_WIDTH / 2, -GET_DESIGN_HEIGHT*.5f), Point(-GET_DESIGN_WIDTH / 2, GET_DESIGN_HEIGHT *.55f));
    createLaserEffect(Point(GET_DESIGN_WIDTH / 2, -GET_DESIGN_HEIGHT *.5f), Point(GET_DESIGN_WIDTH / 2, GET_DESIGN_HEIGHT *.55f));
    

	runAction(RepeatForever::create(Sequence::create(DelayTime::create(1.f), CallFunc::create([this]() {
		timer--;

		if (clockLabel != NULL)
			clockLabel->setString(sformat("%i", timer));

		if (timer < 1)
			finishGame();

	}), NULL)));

	GameManager::getInstance()->setScore1(0);
	GameManager::getInstance()->setScore2(0);
    
    return true;
}


void GameScene1::update(float delta)
{
    auto dt60 = delta*60.f;
    if(dt60>2.f) dt60=2.f;
    
    if(hasEnd) return;
    
    std::vector<int> wavesToRemove;
    for (int i =0; i< allWaves.size(); i++) {
        
        auto wave = allWaves.at(i);
        
        wave->setPosition(wave->getPositionX()+wave->getSpeedX(),wave->getPositionY()+wave->getSpeedY());
        
        float x = wave->getSpeedX(), y = wave->getSpeedY();
        auto waveWidth = wave->waveSprite->getBoundingBox().size.width;
        auto waveHeight = wave->waveSprite->getBoundingBox().size.height;
        
        wave->setPosition(wave->getPositionX() + (x*dt60),  wave->getPositionY() + (y*dt60));
        
        if (wave->getPositionX() < (-GET_DESIGN_WIDTH + waveWidth) * .5f) {
            wave->setPositionX((-GET_DESIGN_WIDTH +  waveWidth) * .5f);
            wave->setRotation( - wave->getRotation() );
            runAction(CCShake::actionWithDuration(.2f, .5f));
            x = -x;
        }
        else if (wave->getPositionX() > (GET_DESIGN_WIDTH - waveWidth) * .5f) {
            wave->setPositionX((GET_DESIGN_WIDTH - waveWidth) * .5f);
            wave->setRotation( - wave->getRotation() );
            runAction(CCShake::actionWithDuration(.2f, .5f));
            x = -x;
        }
        
        if (wave->getPositionY() < -GET_DESIGN_HEIGHT * .5f) {
            
            wave->setRotation( 180 - wave->getRotation() );
            wave->setPositionY(-GET_DESIGN_HEIGHT  * .5f);
            
            y = -y;
        }
        else if (wave->getPositionY() > (GET_DESIGN_HEIGHT  * .5f)- waveHeight) {
            
            wave->setRotation( 180 - wave->getRotation() );
            wave->setPositionY((GET_DESIGN_HEIGHT  * .5f)- waveHeight);
            
            y = -y;
        }
        
        wave->setSpeed(x, y);
        
        
        auto waveRect = Rect(wave->getPositionX()-wave->waveSprite->getBoundingBox().size.width/2,wave->getPositionY()-wave->waveSprite->getBoundingBox().size.height/2,wave->waveSprite->getBoundingBox().size.width,wave->waveSprite->getBoundingBox().size.height);
        
        if(hasCollision(gameBall->getPosition(), ballWidth , wave->getPosition(), wave->waveSprite->getBoundingBox().size.width*.8f)){
            
            
            float forceX=1.5f*wave->getSpeedX()*wave->waveSprite->getOpacity()/255.f,forceY = 1.5f*wave->getSpeedY()*wave->waveSprite->getOpacity()/255;
            
            wave->waveSprite->runAction(FadeOut::create(.2f));
            wave->waveSprite->setOpacity(0);
            
			if (wave->getPlayer() == 1) {
				meteorParticle->setStartColor(Color4F(1.f, 0.15f, 0.1f, 1.f));
			}
			else if (wave->getPlayer() == 2) {
				meteorParticle->setStartColor(Color4F(.2f, 0.8f, 0.1f, 1.f));
			}

            //TODO BULUNAN AÇIYA WAVE AÇISI EKLENECEK
            
            gameBall->applyForce(forceX, forceY);
			gameBall->setLastPlayer(wave->getPlayer());
            
        }
        if(wave->waveSprite->getOpacity() > 3)
        {
            wave->waveSprite->setOpacity(wave->waveSprite->getOpacity() - 3);
            wave->waveSprite->setScale(wave->waveSprite->getScale() + 0.001f);
        }
       else
       {
            wave->waveSprite->setOpacity(0);
       }
        
        
        if(wave->waveSprite->getOpacity() <= 0 )
        {
            wavesToRemove.push_back(i);
        }
        
    }
    
    
    for (int i =0; i< wavesToRemove.size(); i++) {
        
        auto wave = allWaves.at(wavesToRemove.at(i) - i);
        allWaves.erase(allWaves.begin() + wavesToRemove.at(i) - i);
        wave->removeFromParent();
        
    }
    //////
    
    float x = gameBall->getSpeedX(), y = gameBall->getSpeedY();
    
    gameBall->setPosition(gameBall->getPositionX() + (x*dt60),  gameBall->getPositionY() + (y*dt60));
    
    if (gameBall->getPositionX() < (-GET_DESIGN_WIDTH + ballWidth) * .5f) {
        gameBall->setPositionX((-GET_DESIGN_WIDTH +  ballWidth) * .5f);
        
        runAction(CCShake::actionWithDuration(.2f, .8f));
        x = -x;
    }
    else if (gameBall->getPositionX() > (GET_DESIGN_WIDTH - ballWidth) * .5f) {
        gameBall->setPositionX((GET_DESIGN_WIDTH - ballWidth) * .5f);
        
        runAction(CCShake::actionWithDuration(.2f, .8f));
        x = -x;
    }
    
    if (gameBall->getPositionY() < (-GET_DESIGN_HEIGHT + ballWidth) * .5f) {
        gameBall->setPositionY((-GET_DESIGN_HEIGHT + ballWidth) * .5f);
        y = -y;
    }
    else if (gameBall->getPositionY() > (GET_DESIGN_HEIGHT - ballWidth) * .5f) {
        gameBall->setPositionY((GET_DESIGN_HEIGHT - ballWidth) * .5f);
        y = -y;
    }
    
    gameBall->setSpeed(x, y);
    
    //////
    
    
    if(hasCollision(gameBall->getPosition(), ballWidth, bottomPlanet->getPosition(),  bottomPlanet->planetSprite->getBoundingBox().size.width*.6f)){    
        topPlanet->increaseScore();
		GameManager::getInstance()->setScore2(topPlanet->getScore());
        hasEnd = true;
		GameScene1::startAfterScore(delta);
    }
    
    if(hasCollision(gameBall->getPosition(), ballWidth, topPlanet->getPosition(), topPlanet->planetSprite->getBoundingBox().size.width*.6f)){
        bottomPlanet->increaseScore();
		GameManager::getInstance()->setScore1(bottomPlanet->getScore());
        hasEnd = true;
		GameScene1::startAfterScore(delta);
    }
    
    ////
    auto value = cocos2d::random() % 500;
    
    if (value == 1) {
        generateRandomSpaceObject();
    }

	auto value2 = cocos2d::random() % 100;

	if (value2 == 1) {
		generateClockShip();
	}
}

Wave* GameScene1::generateWaveObject(int player)
{
	Wave* wave;

	if (player == 1) {
		wave = Wave::create("res/green-wave.png");
	}
	else if (player == 2) {
		wave = Wave::create("res/purple-wave.png");
	}
  
	wave->setPlayer(player);    
    return wave;
}

void GameScene1::generateRandomSpaceObject() {
    int spaceObjectType = random(0, 7), startHeight = random(0, (int)GET_HEIGHT), endHeight = random(0, (int)GET_HEIGHT), rotation = random(0, 1);
    float scale;
    Sprite* spaceObject;
    
    if (spaceObjectType == 0) {
        spaceObject = Sprite::create("res/satellite.png");
        scale = 0.1f + fmodf(rand_0_1(), 0.35f);
    }
    else if (spaceObjectType == 1) {
        spaceObject = Sprite::create("res/rocket.png");
        scale = 0.1f + fmodf(rand_0_1(), 0.35f);
    }
    else if (spaceObjectType > 1) {
        spaceObject = Sprite::create(sformat("res/back-meteor-%i.png", spaceObjectType - 1));
        scale = 0.3f + fmodf(rand_0_1(), 0.35f);
        spaceObject->runAction(RepeatForever::create(RotateBy::create(1.0f, 40.0f)));
    }
    
    spaceObject->setScale(scale);
    spaceObject->setColor(Color3B(220, 220, 220));
    MoveTo* moveAction;
    auto rotationAngle = atan(((float)endHeight - (float)startHeight) / GET_WIDTH) * 180 / PI;
    
    if (rotation == 0) {
        spaceObject->setPosition(-100, startHeight);
        moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(GET_WIDTH + 100, (float)endHeight));
        spaceObject->setRotation(90 - rotationAngle);
    }
    else {
        spaceObject->setPosition(GET_WIDTH + 100, startHeight);
        moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(-100, (float)endHeight));
        spaceObject->setRotation(-90 + rotationAngle);
    }
    
    spaceObjectsNode->addChild(spaceObject);
    
    spaceObject->runAction(Sequence::create(moveAction, CallFunc::create([this, spaceObject]() {
        spaceObject->removeFromParent();
    }), NULL));
}


bool GameScene1::hasCollision(Vec2 pos1, float width1, Vec2 pos2, float width2)
{
	auto distanceX = pos1.x - pos2.x;
	auto distanceY = pos1.y - pos2.y;

	return sqrt(distanceX*distanceX + distanceY*distanceY) < (width1 + width2)*.5f;
}

void GameScene1::createLaserEffect(Point startPos, Point endPos) {
    laserEffect = new CensLaser();
    laserEffect->autorelease();
    laserEffect->init();
    content->addChild(laserEffect);
    laserEffect->setStart(startPos);
    laserEffect->setEnd(endPos);
}

void GameScene1::startAfterScore(float delta) {

    touchableBottom->releaseWave();
    touchableTop->releaseWave();
    
    runAction(CCShake::actionWithDuration(.4f, 5.f));
    
	ParticleSystemQuad* explosionParticle = ParticleSystemQuad::create("res/particles/explosion.plist");
	explosionParticle->setScale(.8f);
	explosionParticle->setPositionType(ParticleSystem::PositionType::FREE);
	explosionParticle->setPosition(gameBall->getPosition());
	content->addChild(explosionParticle);

	gameBall->runAction(EaseBackOut::create(ScaleTo::create(.7f, 0)));
	meteorParticle->stopSystem();

    CocosDenshion::SimpleAudioEngine::getInstance()->pauseEffect(bottomSoundEffect);
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseEffect(topSoundEffect);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("res/audio/explosion.m4a");
   
	for (int i = 0; i< allWaves.size(); i++) {
		allWaves.at(i)->removeFromParent();
	}
	allWaves.clear();

	gameBall->runAction(Sequence::create(DelayTime::create(1.f), CallFunc::create([this]() {
		gameBall->setPosition(0, 0);
		gameBall->setSpeed(0, 0);
		meteorParticle->resetSystem();
		meteorParticle->setStartColor(Color4F(.5f, .5f, .5f, .15f));
		gameBall->runAction(Sequence::create(EaseBackOut::create(ScaleTo::create(.2f, .9f)), CallFunc::create([this]() {
			gameBall->applyForce(cocos2d::random(0, 100) / 10.f * cocos2d::rand_minus1_1(), cocos2d::random(0, 100) / 10.f * cocos2d::rand_minus1_1());
			hasEnd = false;
			scheduleUpdate();
		}), NULL));
	}), NULL));
}

void GameScene1::generateClockShip() {

	if (clockLabel != NULL) return;

	int startHeight = random(0, (int)GET_HEIGHT), endHeight = random(0, (int)GET_HEIGHT), rotation = random(0, 1);
	Sprite* spaceObject;
	Node* spaceObjectNode;

	spaceObjectNode = Node::create();
	MoveTo* moveAction;
	auto rotationAngle = atan(((float)endHeight - (float)startHeight) / GET_WIDTH) * 180 / PI;

	spaceObject = Sprite::create("res/rocket.png");
	spaceObjectNode->addChild(spaceObject, 2);
	spaceObject->setColor(Color3B(180, 180, 180));


	clockLabel = Label::createWithTTF("90", "fonts/dismay.ttf", 35);
	clockLabel->setTextColor(Color4B(255, 219, 21, 225));
	clockLabel->setPosition(spaceObject->getPositionX(), spaceObject->getPositionY());
	spaceObjectNode->addChild(clockLabel, 3);

	if (rotation == 0) {
		spaceObjectNode->setPosition(-100, startHeight);
		moveAction = MoveTo::create(15.f, Vec2(GET_WIDTH + 300, (float)endHeight));
		spaceObjectNode->setRotation(90 - rotationAngle);
		clockLabel->setRotation(clockLabel->getRotation() - 90);
	}
	else {
		spaceObjectNode->setPosition(GET_WIDTH + 100, startHeight);
		moveAction = MoveTo::create(15.f, Vec2(-300, (float)endHeight));
		spaceObjectNode->setRotation(-90 + rotationAngle);
		clockLabel->setRotation(clockLabel->getRotation() - 90);
	}

	ParticleSystemQuad* sunParticle = ParticleSystemQuad::create("res/particles/rocket-flare.plist");
	//sunParticle->setScale();
	sunParticle->setPositionType(ParticleSystem::PositionType::FREE);
	sunParticle->setPosition(spaceObject->getPositionX() + 5, spaceObject->getPositionY() - 100);
	sunParticle->setRotation(spaceObject->getRotation());
	spaceObjectNode->addChild(sunParticle, 1);

	spaceObjectsNode->addChild(spaceObjectNode);

	spaceObjectNode->runAction(Sequence::create(moveAction, CallFunc::create([this, spaceObject, sunParticle]() {
		spaceObject->removeFromParent();
		clockLabel->removeFromParent();
		sunParticle->removeFromParent();
		clockLabel = NULL;
	}), NULL));
}

void GameScene1::finishGame() {
    
    unscheduleUpdate();
    touchableBottom->removeListener();
    touchableTop->removeListener();
    
	auto scene = GameEndScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
}
