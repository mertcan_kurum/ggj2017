#include "ButtonImageWithText.h"
#include "Defines.h"


static bool customFont=false;
std::string customFontName;

ButtonImageWithText* ButtonImageWithText::create(std::string fontNameCustom)
{
    customFont=true;
    customFontName=fontNameCustom;
    
    return ButtonImageWithText::create();
}

bool ButtonImageWithText::init()
{
    //if(!customFont) fontName = FONT("grobold.ttf");
    //else fontName = customFontName;
	fontName = "fonts/hunger-games.ttf";
    customFont=false;
    
    return init(fontName);
}

bool ButtonImageWithText::init(std::string font)
{
    auto ret = ButtonImage::init();
    
    fontName = font;
    
    shadowColor = Color4B(54, 42, 60, 255);
    shadowSize = Size(GET_HEIGHT*.005, -GET_HEIGHT*.005);
    
    label = Label::createWithTTF("", fontName, 0);
    label->setTextColor(Color4B(0,0,0,225));
    label->enableShadow(shadowColor,shadowSize,0);
    
    mNode->addChild(label);
    
    return ret;
}

void ButtonImageWithText::setOpacity(int value)
{
    label->setOpacity(value);
    ButtonImage::setOpacity(value);
}

void  ButtonImageWithText::setLabelOffset(Vec2 offset)
{
    label->setPosition(offset);
}

void ButtonImageWithText::labelFitH(float height)
{
    
    
    TTFConfig ttfConfig(fontName.c_str(), height);
    label->setTTFConfig(ttfConfig);
    
    hasAdditionalTouchRect=true;
    additionalTouchRectNode=label;
    
}

void ButtonImageWithText::setLabelShadowColor(Color4B color)
{
    label->enableShadow(color,Size(GET_HEIGHT*.0025, -GET_HEIGHT*.0025),0);
    
    shadowColor = color;
    shadowSize = Size(GET_HEIGHT*.0025, -GET_HEIGHT*.0025);
}
