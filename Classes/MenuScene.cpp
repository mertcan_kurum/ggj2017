#include "MenuScene.h"
#include "GameScene1.h"
#include "Defines.h"
#include "ButtonImageWithText.h"
#include "SimpleAudioEngine.h"


cocos2d::Scene* MenuScene::createScene()
{
	auto scene = Scene::create();

	auto layer = MenuScene::create();

	scene->addChild(layer);

	return scene;
}

bool MenuScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

    
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/menu-sound.m4a",true);
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(.2f);
    
	auto bg = Sprite::create("res/game-bg.png");
	bg->setPosition(GET_WIDTH / 2, GET_HEIGHT / 2);
	
	addChild(bg);

	ParticleSystemQuad* particle1 = ParticleSystemQuad::create("res/particles/firefly-particle.plist");
	particle1->setPosVar(Vec2(GET_WIDTH, GET_HEIGHT));
	particle1->setColor(Color3B(255, 0, 0));
	addChild(particle1);

	auto planet1 = Sprite::create("res/planet-1.png");
	planet1->setPosition(GET_WIDTH * 0.02f, +GET_HEIGHT * 0.02f);
	planet1->runAction(RepeatForever::create(RotateBy::create(1.0f, 1.0f)));
	addChild(planet1, 5);

	auto planet2 = Sprite::create("res/planet-2.png");
	planet2->setPosition(GET_WIDTH * 0.7f, GET_HEIGHT * 0.7f);
	planet2->runAction(RepeatForever::create(RotateBy::create(1.0f, -2.0f)));
	addChild(planet2, 5);


	auto logo = Sprite::create("res/logo.png");
	logo->setPosition(GET_WIDTH / 2, GET_HEIGHT + 300);
	logo->runAction(Sequence::create(DelayTime::create(1.0f), EaseBackOut::create(MoveTo::create(0.7f, Vec2(GET_WIDTH / 2, GET_HEIGHT * .75f))),NULL));
	addChild(logo, 10);

	std::string fragSource = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("res/shaders/flickering.fsh"));
	auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource.data());

	auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
	logo->setGLProgramState(glProgramState);
	logo->getGLProgramState()->setUniformFloat("u_gtime", 3.0f);
	logo->getGLProgramState()->setUniformVec4("u_color", Vec4(0.5f, 0.5f, 0.5f, 1.0f));
	logo->getGLProgramState()->setUniformFloat("u_delay", 5.0f);

	auto startButton = ButtonImageWithText::create();
	startButton->ButtonImage::init("res/button.png");
	startButton->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.4f);
	startButton->label->setString("Start");
	startButton->setLabelOffset(Vec2(startButton->mSprite->getBoundingBox().size.width * .1f, -startButton->mSprite->getBoundingBox().size.height * .02f));
	startButton->label->setTextColor(Color4B(255, 219, 21, 225));
	startButton->setScale(0);
	startButton->labelFitH(startButton->mSprite->getBoundingBox().size.height * .4f);
	startButton->runAction(Sequence::create(DelayTime::create(1.3f),EaseBackOut::create(ScaleTo::create(.3f, 0.7f)),NULL));
	addChild(startButton, 10);

	auto creditsButton = ButtonImageWithText::create();
	creditsButton->ButtonImage::init("res/button.png");
	creditsButton->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.22f);
	creditsButton->label->setString("Credits");
	creditsButton->setLabelOffset(Vec2(startButton->mSprite->getBoundingBox().size.width * .1f, -startButton->mSprite->getBoundingBox().size.height * .02f));
	creditsButton->label->setTextColor(Color4B(255, 219, 21, 225));
	creditsButton->labelFitH(startButton->mSprite->getBoundingBox().size.height * .4f);
	creditsButton->setScale(0);
	creditsButton->runAction(Sequence::create(DelayTime::create(1.4f), EaseBackOut::create(ScaleTo::create(.3f, 0.7f)), NULL));
	addChild(creditsButton, 10);

	auto creditsNode = Node::create();
	creditsNode->setPosition(0, GET_HEIGHT);
	addChild(creditsNode, 100);

	auto creditsNodeBackground = Sprite::create("res/credits-bg.png");
    creditsNodeBackground->setScale(1.2f);
	creditsNodeBackground->setPosition(GET_WIDTH / 2, GET_HEIGHT*.6f);
	creditsNodeBackground->setColor(Color3B(120, 120, 120));
	creditsNode->addChild(creditsNodeBackground);

	ParticleSystemQuad* particle2 = ParticleSystemQuad::create("res/particles/firefly-particle.plist");
	particle2->setPosVar(Vec2(GET_WIDTH, GET_HEIGHT));
	particle2->setColor(Color3B(255, 0, 0));
	particle2->setTotalParticles(200);
	creditsNode->addChild(particle2);

	auto label1 = Label::createWithTTF("Mert Can KURUM", "fonts/hunger-games.ttf", 36);
	label1->setTextColor(Color4B(255, 219, 21, 225));
	label1->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.8f);
	creditsNode->addChild(label1);

	auto label2 = Label::createWithTTF("ismail hatip", "fonts/hunger-games.ttf", 36);
	label2->setTextColor(Color4B(255, 219, 21, 225));
	label2->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.7f);
	creditsNode->addChild(label2);

	auto label3 = Label::createWithTTF("canberk ulas", "fonts/hunger-games.ttf", 36);
	label3->setTextColor(Color4B(255, 219, 21, 225));
	label3->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.6f);
	creditsNode->addChild(label3);

	auto label4 = Label::createWithTTF("erda baykara", "fonts/hunger-games.ttf", 36);
	label4->setTextColor(Color4B(255, 219, 21, 225));
	label4->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.5f);
	creditsNode->addChild(label4);

	auto label5 = Label::createWithTTF("murat okar", "fonts/hunger-games.ttf", 36);
	label5->setTextColor(Color4B(255, 219, 21, 225));
	label5->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.4f);
	creditsNode->addChild(label5);

	auto label6 = Label::createWithTTF("", "fonts/hunger-games.ttf", 36);
	label6->setTextColor(Color4B(255, 219, 21, 225));
	label6->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.3f);
	creditsNode->addChild(label6);

	auto creditsBackButton = ButtonImageWithText::create();
	creditsBackButton->ButtonImage::init("res/button.png");
	creditsBackButton->setPosition(GET_WIDTH / 2, GET_HEIGHT * 0.15f);
	creditsBackButton->label->setString("Back");
	creditsBackButton->setLabelOffset(Vec2(startButton->mSprite->getBoundingBox().size.width * .1f, -startButton->mSprite->getBoundingBox().size.height * .02f));
	creditsBackButton->label->setTextColor(Color4B(255, 219, 21, 225));
	creditsBackButton->setScale(0);
	creditsBackButton->labelFitH(startButton->mSprite->getBoundingBox().size.height * .4f);
	creditsBackButton->runAction(Sequence::create(DelayTime::create(1.3f), EaseBackOut::create(ScaleTo::create(.3f, 0.7f)), NULL));
	creditsBackButton->setPriority(-2);
	creditsNode->addChild(creditsBackButton, 100);

	startButton->onTap([this, planet1, planet2, logo, startButton, creditsButton](ButtonImage *sender) {
		sender->mSprite->stopAllActions();
	
        logo->runAction(EaseBackIn::create(MoveTo::create(.5f, Vec2(GET_WIDTH / 2, GET_HEIGHT + 300))));
		startButton->runAction(EaseBackIn::create(ScaleTo::create(.5f, 0)));
		creditsButton->runAction(EaseBackIn::create(ScaleTo::create(.5f, 0)));

		planet1->runAction(EaseOut::create(Spawn::createWithTwoActions((MoveTo::create(.8f, Vec2(GET_WIDTH / 2, 0))), ScaleTo::create(.75f, .7f)), 2.0f));
		planet2->runAction(EaseOut::create(Spawn::createWithTwoActions((MoveTo::create(.8f, Vec2(GET_WIDTH / 2, GET_HEIGHT))), ScaleTo::create(.75f, .7f)), 2.0f));

		auto touchAnimationBottom = Sprite::create("res/touch-field.png");
		FIT_W(touchAnimationBottom, GET_DESIGN_WIDTH*1.11f);
		touchAnimationBottom->setPosition(GET_WIDTH / 2, 0);

		auto initialScaleBottom = touchAnimationBottom->getScale();
		touchAnimationBottom->setScale(0);
		addChild(touchAnimationBottom);
		touchAnimationBottom->runAction(Sequence::create(DelayTime::create(.3f), EaseBackIn::create(ScaleTo::create(0.4f, initialScaleBottom)),NULL));

		auto touchAnimationTop = Sprite::create("res/touch-field.png");
		FIT_W(touchAnimationTop, GET_DESIGN_WIDTH*1.11f);
		auto initialScaleTop = touchAnimationTop->getScale();

		touchAnimationTop->setPosition(GET_WIDTH / 2, GET_HEIGHT);
		touchAnimationTop->setScale(0);
		addChild(touchAnimationTop);
		touchAnimationTop->runAction(Sequence::create(DelayTime::create(.3f), EaseBackIn::create(ScaleTo::create(0.4f, initialScaleTop)), NULL));

		this->scheduleOnce(schedule_selector(MenuScene::startGame), .9f);
	});

	creditsButton->onTap([this, creditsNode, startButton, creditsButton](ButtonImage *sender) {
		creditsNode->runAction(EaseBackOut::create(MoveTo::create(0.5f, Vec2(0,0))));
		startButton->setVisible(false);
		creditsButton->setVisible(false);
	});

	creditsBackButton->onTap([this, creditsNode, startButton, creditsButton](ButtonImage *sender) {
		creditsNode->runAction(EaseBackIn::create(MoveTo::create(0.4f, Vec2(0, GET_HEIGHT))));
		startButton->setVisible(true);
		creditsButton->setVisible(true);
	});

	scheduleUpdate();
	return true;
}

void MenuScene::update(float delta) {
	auto value = cocos2d::random() % 500;

	if (value == 1) {
		generateRandomSpaceObject();
	}
}

void MenuScene::generateRandomSpaceObject() {

	int spaceObjectType = random(0, 7), startHeight = random(0, (int)GET_HEIGHT), endHeight = random(0, (int)GET_HEIGHT), rotation = random(0, 1);
	float scale;
	Sprite* spaceObject;

	if (spaceObjectType == 0) {
		spaceObject = Sprite::create("res/satellite.png");
		scale = 0.1f + fmodf(rand_0_1(), 0.15f);
	}
	else if (spaceObjectType == 1) {
		spaceObject = Sprite::create("res/rocket.png");
		scale = 0.1f + fmodf(rand_0_1(), 0.25f);
	}
	else if (spaceObjectType > 1) {
		spaceObject = Sprite::create(sformat("res/back-meteor-%i.png", spaceObjectType - 1));
		scale = 0.3f + fmodf(rand_0_1(), 0.35f);
		spaceObject->runAction(RepeatForever::create(RotateBy::create(1.0f, 40.0f)));
	}

	spaceObject->setScale(scale);
	spaceObject->setColor(Color3B(182, 182, 182));
	MoveTo* moveAction;
	auto rotationAngle = atan(((float)endHeight - (float)startHeight) / GET_WIDTH) * 180 / PI;

	if (rotation == 0) {
		spaceObject->setPosition(-100, startHeight);
		moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(GET_WIDTH + 100, (float)endHeight));
		spaceObject->setRotation(90 - rotationAngle);
	}
	else {
		spaceObject->setPosition(GET_WIDTH + 100, startHeight);
		moveAction = MoveTo::create(1 / scale * 3.0f, Vec2(-100, (float)endHeight));
		spaceObject->setRotation(-90 + rotationAngle);
	}

	if (scale < 0.2f) {
		addChild(spaceObject, 3);
	}
	else {
		addChild(spaceObject, 6);
	}

	spaceObject->runAction(Sequence::create(moveAction, CallFunc::create([this, spaceObject]() {
		spaceObject->removeFromParent();
	}), NULL));
}


void MenuScene::startGame(float dt) {
	auto scene = GameScene1::createScene();
	Director::getInstance()->replaceScene(TransitionProgressVertical::create(0.3f, scene));
}
