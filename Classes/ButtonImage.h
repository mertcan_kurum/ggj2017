#ifndef __OnlineHeadBall__ButtonImage__
#define __OnlineHeadBall__ButtonImage__

#include "cocos2d.h"

USING_NS_CC;

class ButtonImage: public Node
{
    
public:
    
    static void touchedInsideMaskedArea();
    static void touchedOutsideMaskedArea();
    
    virtual bool init();
    
    void disable();
    
    void fitWH(float width,float height);
    void fitH(float height);
    
    bool mPress = false;
    int mOpacity = 255;
    float pressScaleFactor=1.f;
    
    std::function<void(ButtonImage* sender)> mCallbackHover = NULL;
    std::function<void(ButtonImage* sender)> mCallbackPress = NULL;
    std::function<void(ButtonImage* sender)> mCallbackRelease = NULL;
    std::function<void(ButtonImage* sender)> mCallbackTap = NULL;
    std::function<bool(ButtonImage* sender)> mCallbackPressFilter = NULL;
    
    float mWidth = 0;
    float mHeight = 0;
    
    Node* mNode;
    Sprite* mSprite;
    
    //
    
    void onTap(std::function<void(ButtonImage* sender)> value);
    void onHover(std::function<void(ButtonImage* sender)> value);
    void onRelease(std::function<void (ButtonImage *)> value);
    void onPress(std::function<void(ButtonImage* sender)> value);
    void onPressFilter(std::function<bool(ButtonImage* sender)> value);
    
    void cancelTouch();
    void setPriority(int value);

    void setTexture(std::string file);
    void setTexture(std::string file, cocos2d::Rect rect);
    
    void width(float value);
    void height(float value);
    
    bool isVisible();
    
    void setOpacity(float value);
    float getOpacity();
    
    bool isInMaskedArea=false;
    
    Texture2D* mTex=NULL;
    cocos2d::Rect mRect;
    cocos2d::Rect mRectHover;
    
    ButtonImage* init(std::string file);
    ButtonImage* init(std::string file, cocos2d::Rect rect);
    ButtonImage* init(std::string file, cocos2d::Rect rect,cocos2d::Rect rectHover);
    
    void changeTexture(std::string file);
    void changeTexture(std::string file, cocos2d::Rect rect);
    void changeTexture(std::string file, cocos2d::Rect rect,cocos2d::Rect rectHover);
    
    void setTextureRect(cocos2d::Rect rect);

    bool hasAdditionalTouchRect=false;
    Node* additionalTouchRectNode;
    
    bool doNotAnimate = false;
    bool doNotPlaySoundOnRelease = false;
    
    CREATE_FUNC(ButtonImage);
    
private:
    
    void _onRelease();
    bool disabled=false;
    
    double currentTimeMilliseconds;
    Vec2 location;
    
    bool canPressButton();
    
    void update(float dt);
    
    EventListenerTouchOneByOne* listener;
    
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    cocos2d::Touch* currentTouch;

    virtual void onEnter();
    virtual void onExit();
  
};

#endif
